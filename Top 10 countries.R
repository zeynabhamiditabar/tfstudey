
#install.packages(shiny)
library(shiny)
library(ggplot2)



variance <- list()

for(i in seq(1,6468, by=28)){
  country.name <- death_rates_smoking_age$Entity[i]
  country.var <- var(death_rates_smoking_age$`All ages (deaths per 100,000)`[i:(i+27)])
  name_var <- list(list(name= country.name, var = country.var))
  variance <- append(variance, name_var)
}
sorted.variance <-  variance[order(sapply(variance, "[[", "var"), decreasing = TRUE)]

top.5.countries <-sorted.variance[1:5]

###############################################

average <- list()

for(i in seq(1,6468, by=28)){
  country.name <- death_rates_smoking_age$Entity[i]
  country.average <- mean(death_rates_smoking_age$`All ages (deaths per 100,000)`[i:(i+27)])
  name_averge <- list(list(name= country.name, average = country.average))
  average <- append(average, name_averge)
}
sorted.mean <-  average[order(sapply(average, "[[", "average"), decreasing = TRUE)]

top.10.countries <-sorted.mean[1:10]

names<-c()
for(i in 1:10){
  names<- append(names, top.10.countries[[i]][[1]])
}

(names)
#[1] "Hungary"        "Bulgaria"       "Scotland"       "Denmark"        "Ukraine"       
#[6] "Belarus"        "Serbia"         "Croatia"        "Wales"          "Eastern Europe"

library(ggplot2)
#install.packages("ggplot2")
library(reshape)
library(dplyr)

#################################


Scotland <- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Scotland")]
Hungary<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Hungary")]
Bulgaria<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Bulgaria")]
Denmark<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Denmark")]
Ukraine<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity== "Ukraine")]
Belarus<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Belarus")]
Serbia<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Serbia" )]
Croatia<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Croatia")]
Wales<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Wales")]
Eastern_Europe<- death_rates_smoking_age$`All ages (deaths per 100,000)`[which(death_rates_smoking_age$Entity=="Eastern Europe")]

year <- (1990: 2017)

plot(year,  Scotland,ylim=c(50,400), type = "o",col = "red", xlab = "Year", ylab = "Death",
     main = "Death Rate by Year")
lines(year, Hungary, type = "o", col = "blue")
lines(year, Denmark, type = "o", col = "green")
lines(year, Ukraine, type = "o", col = "pink")
lines(year, Belarus, type = "o", col = "yellow")
lines(year, Serbia, type = "o", col = "black")
lines(year, Croatia, type = "o", col = "purple")
lines(year, Wales, type = "o", col = "orange")
lines(year, Eastern_Europe, type = "o", col = "brown")
lines(year, Bulgaria, type = "o", col = "grey")


#######################


